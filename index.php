<?php require_once "./code.php" ?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>S04: Access Modifiers and Encapsulation</title>
</head>

<body>
    <h1>Access Modifiers</h1>

    <h2>Building Object</h2>
    <p><?php //echo $building->name; 
        ?></p>

    <!-- You will received a "Warning: Undefined property: Condominium::$name". -->
    <!-- This is because Condominium no longer inherits the "name" property due to its "private" modifier in the Building class. -->

    <!-- If the modifier is changed again to protected an " Uncaught Error: Cannot access protected property Condominium::$name" -->
    <!-- This is because we cannot directly access properties with protected access modifier, but compare with the previous warning displayed in the browser we are now able to inherit building properties. -->


    <h1>Building</h1>
    <p><?php echo $building->getName(); ?></p>



    <!-- Activity codes here -->

    <h1>Condominium</h1>
    <p><?php echo $condominium->getName(); ?></p>
    <p><?php echo $condominium->setName("", "", "Enzo Tower"); ?></p>



    <!-- Activity codes here -->





</body>

</html>